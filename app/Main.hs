{-# LANGUAGE StandaloneDeriving, TypeFamilies, TypeApplications, DataKinds, TypeOperators, GADTs, KindSignatures, PolyKinds, TemplateHaskell, ScopedTypeVariables, FlexibleContexts, LambdaCase, GeneralizedNewtypeDeriving, RankNTypes, UndecidableInstances #-}
{-# OPTIONS_GHC -fplugin=Polysemy.Plugin #-}
module Main where

import GHC.TypeLits
import Polysemy
import Polysemy.State
import RIO hiding (over)
import Data.Functor.Const
import RIO.Lens
import Lens.Micro.TH
import Data.Kind

view :: MemberWithError (State s) r => Getting a s a -> Sem r a
view l = getConst <$> gets (l Const)

(%=) :: MemberWithError (State s) r => ASetter s s a b -> (a -> b) -> Sem r ()
l %= f = modify (l %~ f)

(+=) :: (MemberWithError (State s) r, Num a) => ASetter s s a a -> a -> Sem r ()
l += x = l %= (+ x)

(*=) :: (MemberWithError (State s) r, Num a) => ASetter s s a a -> a -> Sem r ()
l *= x = l %= (* x)

test :: Member (State (Int, Int)) r => Sem r ()
test = do
  _1 += 232
  _2 *= 6

runTest = run $ execState (1, 111) test

newtype Gold = Gold Int
  deriving (Show, Num)

newtype Fish = Fish Int
  deriving Num

newtype Wood = Wood Int
  deriving Num

newtype Share = Share Player

newtype UnissuedShare = UnissuedShare Player

data Player = Red | Green | Blue | White | Yellow

data NatCounter s m a where
  Plus    :: Num s => s -> NatCounter s m ()
  Minus   :: Num s => s -> NatCounter s m ()
  ReportN :: Num s => s -> NatCounter s m ()
  ToZero  :: Num s => NatCounter s m s

data ResourceCounter s m a where
  Add    :: s -> ResourceCounter s m ()
  Remove :: s -> ResourceCounter s m ()

makeSem ''NatCounter

makeSem ''ResourceCounter

newtype (:::) (s :: Symbol) (a) = Val { getVal :: a}

deriving instance Num        a => Num        (s ::: a)

data PlayerBoard n o p q m a where
  InReserve  :: Sem n a -> PlayerBoard n o p q m a
  InSupply   :: Sem o a -> PlayerBoard n o p q m a
  InSpaces   :: Sem p a -> PlayerBoard n o p q m a
  InElderRow :: Sem q a -> PlayerBoard n o p q m a

makeSem ''PlayerBoard

type SupplyK  = '[NatCounter Gold, NatCounter Wood, NatCounter Fish]
type ReserveK = '[NatCounter Gold, NatCounter Wood, NatCounter Fish]
type EldersK  = '[]
type SpacesK  = '[]

type NusfjordPlayerBoard = PlayerBoard ReserveK SupplyK SpacesK EldersK

prog :: (Member (NatCounter Gold) n,
         Member (PlayerBoard n o p q) m) => Sem m ()
prog = do
  inReserve $ plus @Gold 5

transferReserve :: Member NusfjordPlayerBoard m => Sem m ()
transferReserve = do
  g <- inReserve $ toZero @Gold
  h <- inReserve $ toZero @Wood
  i <- inReserve $ toZero @Fish
  inSupply $ plus @Gold g
  inSupply $ plus @Wood h
  inSupply $ plus @Fish i

{--
data PlayerSupply m a where
  PlusGold           :: Gold -> PlayerSupply m ()
  MinusGold          :: Gold -> PlayerSupply m ()
  PlusFish           :: Fish -> PlayerSupply m ()
  MinusFish          :: Fish -> PlayerSupply m ()
  PlusWood           :: Wood -> PlayerSupply m ()
  MinusWood          :: Wood -> PlayerSupply m ()
  PlusShare          :: Share -> PlayerSupply m ()
  MinusUnissuedShare :: PlayerSupply m ()

makeSem ''PlayerSupply
--}
data Supply = Supply {
  _gold           :: Gold
, _fish           :: Fish
, _wood           :: Wood
, _shares         :: [Share]
, _unissuedshares :: [UnissuedShare]
}

stateToStateViaLens :: Member (State bigSt) r
                    => Lens' bigSt smallSt
                    -> Sem (State smallSt ': r) a
                    -> Sem r a
stateToStateViaLens lens = interpret $ \case
  Put smallSt -> modify' (set lens smallSt)
  Get -> gets (RIO.view lens)

makeLenses ''Supply

interpretNatCounter :: Member (State k) effs => Sem (NatCounter k : effs) a -> Sem effs a
interpretNatCounter = interpret $ \case
  Plus x -> id += x

{--
interpretPlayerSupply :: Member (State Supply) effs => Sem (PlayerSupply : effs) a -> Sem effs a
interpretPlayerSupply = interpret $ \case
  PlusGold x  -> gold += x
data Elder (s :: Symbol) m a = Elder [PlayerSupply m a]

silviculturalist :: Elder "Silviculturalist" m ()
silviculturalist = Elder [PlusWood 1]

--}
test2 :: Members '[NatCounter Gold, NatCounter Wood] r => Sem r ()
test2 = do
  plus @Gold 5

main :: IO ()
main = print $ run $ execState (Gold 0) . execState (Wood 0) . interpretNatCounter @Gold . interpretNatCounter @Wood $ test2
